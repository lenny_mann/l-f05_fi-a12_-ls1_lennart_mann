import java.util.ArrayList;

public class raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private ArrayList<Ladung> Ladung = new ArrayList<Ladung>();
	
	public raumschiff() {
		
	}
	
	public raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, 
					int schildeInProzent) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void addLadung(Ladung neueLadung) {
		Ladung.add(neueLadung);
		
	}
}
