
public class ÜbungAnwendungSchleifen {
//  Schreibe ein Programm, das für alle Zahlen zwischen 1 und 200 testet:
//  ob sie durch 7 teilbar sind.
//  nicht durch 5 aber durch 4 teilbar sind.
//  Lasse jeweils die Zahlen, auf die die Bedingungen zutreffen ausgeben.
	public static void main(String[] args) {
	
		for(int i = 1; i<=200; i ++) {
			if( i % 7 == 0) {
				System.out.println(i+ "ist durch 7 teilbar.");
			}
			if(i % 4 == 0 && i % 5 > 0) {
				System.out.println(i + "ist nicht durch 5, aber durch 4 teilbar");
			}
		}

	}

}
