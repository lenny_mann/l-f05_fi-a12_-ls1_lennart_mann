import java.util.Scanner;

public class Auswahlstrukturen_Aufgabe3 {
//	Aufgabe 3: Hardware-Gro�h�ndler   
//	Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer Bestellung f�r PC-M�use mit einem Programm ermitteln. 
//	Wenn der Kunde mindestens 10 M�use bestellt, erfolgt die Lieferung frei Haus, 
//	bei einer geringeren Bestellmenge wird eine feste Lieferpauschale von 10 Euro erhoben.
//	Vom Gro�h�ndler werden die Anzahl der bestellten M�use und der Einzelpreis eingegeben. Das Programm soll den Rechnungsbetrag (incl. MwSt.) ausgeben.
	
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.println("Wieviel M�use willst du bestellen? ");
		int anzahlM�use = eingabe.nextInt();
		
		System.out.println("Geben sie den Einzelpreis der M�use an: ");
		int einzelpreis = eingabe.nextInt();
		
		if(anzahlM�use >= 10) {
			System.out.println("Sie sind qualifiziert f�r kostenlose Lieferung");
			double rechnungKostenlos = (anzahlM�use * einzelpreis)*1.19;
			System.out.println("Ihr Rechnungsbetrag (incl. MwSt): "+ rechnungKostenlos+("�"));
		}	
		else {
			System.out.println("Ihre Lieferkosten betragen 10 Euro ");
			double rechnungLieferkosten = (anzahlM�use * einzelpreis)*1.19+10;
			System.out.printf("Ihr Rechnungsbetrag (incl. MwSt): %.2f%s ", rechnungLieferkosten,("�"));
		}
		
		
		


		
	}

	
}
