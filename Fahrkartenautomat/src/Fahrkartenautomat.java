﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       boolean endlos = true;
      
       while(endlos = true) {
       double Ticketpreis1 = fahrkartenbestellungErfassen();
       double Bezahlen = fahrkartenBezahlen(Ticketpreis1);
       fahrkartenAusgeben();
       double rückgeld = rueckgeldAusgeben(Bezahlen, Ticketpreis1);}
       
       


    }
    
public static double fahrkartenbestellungErfassen() {
	Scanner tastatur = new Scanner(System.in);
	
//	System.out.print("Zu zahlender Betrag (EURO):\n ");
//    double zuZahlenderBetrag = tastatur.nextDouble();
    
    System.out.println("\n\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n\n"
    		+ "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    		+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    		+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n"
    		);
    double preis = 0;
    boolean richtigeWahl = true;
    while(richtigeWahl = true) {
    	System.out.println("Ihre Wahl:");
    int ticketWahl = tastatur.nextInt();
    if(ticketWahl == 1) {
    	preis = 2.90;
    	break;
    }
    if(ticketWahl == 2) {
    	preis = 8.60;
    	break;
    }
    if(ticketWahl == 3) {
    	preis = 23.50;
    	break;
    }
    if(ticketWahl >3||ticketWahl <1) {
    	System.out.println(">>>FEHLER<<<");
    	richtigeWahl = true;
    }}
    
    
    System.out.print("Anzahl der Tickets: ");
    int anzahlTickets = tastatur.nextInt();
    if (anzahlTickets > 10 || anzahlTickets ==0) {
    	System.out.println("Du hast die Grenze an Tickets erreicht.\nDu machst mit einem Ticket weiter. ");
  	anzahlTickets = 1;
    	
    }
    else if (anzahlTickets<0) {
    	System.out.println("Du hast eine negative Zahl eingegeben, dass geht nicht.\nDu machst mit einem Ticket weiter");
    	anzahlTickets = 1;
    }
    
    double Ticketpreis = anzahlTickets*preis;
    
    return Ticketpreis;
}
	
public static double fahrkartenBezahlen(double zuZahlen) {
	Scanner tastatur = new Scanner(System.in);
	String x = "EURO";
	double eingezahlterGesamtbetrag;
	
	eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < zuZahlen)
    {
 	   System.out.printf("Noch zu zahlen: %.2f %s ",  (zuZahlen - eingezahlterGesamtbetrag) , x);
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   double eingeworfeneMünze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneMünze;
        
       
    }
	return eingezahlterGesamtbetrag;
	
	
}

public static void fahrkartenAusgeben() {
	
	System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
    	warte(250);}
   
    System.out.println("\n\n");

	
	
	
}

public static double rueckgeldAusgeben(double Bezahlen, double Ticketpreis1) {
	double rückgabebetrag;
	rückgabebetrag = Bezahlen - Ticketpreis1;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s", rückgabebetrag ,"EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }

	
	
}
    return rückgabebetrag;
}
public static void warte(int millisekunde){
	System.out.print("=");
    try {
			Thread.sleep(millisekunde);} catch (InterruptedException e) {
			
			e.printStackTrace();}
    
    
 }
 
	
}


