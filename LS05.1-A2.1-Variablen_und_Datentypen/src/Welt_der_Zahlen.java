/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class Welt_der_Zahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 100000000000l ;
    
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 365000000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     short alterTage = 8395 ; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = 200000 ;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
     float flaecheGroessteLand = 17.1f ;
    
    // Wie gro� ist das kleinste Land der Erde?
    
     double flaecheKleinsteLand = 0.44 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne );
    System.out.println("Anzahl Einwohner von Berlin: " + bewohnerBerlin);
    System.out.println("23 Jahre in Tagen: " + alterTage);
    System.out.println("Schwerste Tier in Kg: " + gewichtKilogramm);
    System.out.println("Das gr��te Land in Km�: " + flaecheGroessteLand + "Mio");
    System.out.println("Das kleinste Land in Km�: " + flaecheKleinsteLand);
    
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}